SOURCES += \
	src/main.cpp

CONFIG += link_pkgconfig
CONFIG -= qt core gui
TEMPLATE = app

debug {
	DEFINES += DEBUG
}

linux-* {
	CONFIG += console
	QMAKE_CXXFLAGS += -std=c++11
	LIBS += -lGL -lGLU -lGLEW -lSDL2
}

macx {
	CONFIG += console
	QMAKE_CXXFLAGS += -std=c++11

	#CONFIG -= x86_64
	#CONFIG += x86
	#CONFIG -= app_bundle

	#LIBS += -L$${PWD}/lib/Trieng/lib/BASS/osx/
	#INCLUDEPATH += /Library/Frameworks/SDL2.framework/Headers
	#INCLUDEPATH += /Library/Frameworks/SDL2_image.framework/Headers
	#INCLUDEPATH += /Library/Frameworks/SDL2_ttf.framework/Headers
	#LIBS += -F/Library/Frameworks

	INCLUDEPATH += "/usr/local/include"
	LIBS += -L/usr/local/lib
	LIBS += -framework OpenGL
	LIBS += -framework Cocoa -framework SDL2

	INCLUDEPATH += /Library/Frameworks/SDL2.framework/Headers
	LIBS += -F/Library/Frameworks

	#PKGCONFIG += SDL2_image SDL2_ttf
	#LIBS += -framework,Cocoa -framework,OpenGL
	#LIBS += -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2main -lbass -lm
	#LIBS += -L/usr/local/lib
	#LIBS += -lGLEW -lSDL2 -lSDL2_image -lSDL2_ttf
	#LIBS += -lm -liconv -Wl,-framework,OpenGL -Wl,-framework,ForceFeedback -lobjc
	#LIBS += -Wl,-framework,Cocoa -Wl,-framework,Carbon -Wl,-framework,IOKit -Wl,-framework,CoreAudio
	#LIBS += -Wl,-framework,AudioToolbox -Wl,-framework,AudioUnit -lSDL2main

	#LIBS += -lbass
	#PKGCONFIG += pangocairo
}

win* {
	# You may need to edit DAHLIA_VC to match your installation.
	DAHLIA_VC = "E:\code\libraries\vc2012"
	INCLUDEPATH += "$$DAHLIA_VC\include"

	# SDL & SDL_image & SDL_ttf
	INCLUDEPATH += "$$DAHLIA_VC/include/SDL2"

	CONFIG += console
	#QMAKE_CXXFLAGS += -std=c++11
}

win32:CONFIG(debug, debug|release) {
	LIBS += -L\"$$DAHLIA_VC\lib\Win32\Debug\" -lSDL2main -lSDL2 -lglew32d -lglu32 -lopengl32

	DLL_DIR = "$$DAHLIA_VC\dll\Win32\Debug"
	DESTDIR = "$$OUT_PWD\builds\Win32\Debug"
	MOC_DIR = "$$OUT_PWD\Win32\Debug"
	#QMAKE_POST_LINK += $(MKDIR) \"$$DESTDIR\data\\\"
	QMAKE_POST_LINK += COPY /y \"$$DLL_DIR\glew32d.dll\" \"$$DESTDIR\"
	QMAKE_POST_LINK += && COPY /y \"$$DLL_DIR\SDL2.dll\" \"$$DESTDIR\"
}

win32:CONFIG(release, debug|release) {
	LIBS += -L\"$$DAHLIA_VC\lib\Win32\Release\" -lSDL2main -lSDL2 -lglew32 -lglu32 -lopengl32

	DLL_DIR = "$$DAHLIA_VC\dll\Win32\Release"
	DESTDIR = "$$OUT_PWD\builds\Win32\Release"
	MOC_DIR = "$$OUT_PWD\Win32\Release"
	#QMAKE_POST_LINK += $(MKDIR) \"$$DESTDIR\data\\\"
	QMAKE_POST_LINK += COPY /y \"$$DLL_DIR\glew32.dll\" \"$$DESTDIR\"
	QMAKE_POST_LINK += && COPY /y \"$$DLL_DIR\SDL2.dll\" \"$$DESTDIR\"
}

win64:CONFIG(debug, debug|release) {
	LIBS += -L\"$$DAHLIA_VC\lib\x64\Debug\" -lSDL2main -lSDL2 -lglew32d -lglu32 -lopengl32

	DLL_DIR = "$$DAHLIA_VC\dll\x64\Debug"
	DESTDIR = "$$OUT_PWD\builds\x64\Debug"
	MOC_DIR = "$$OUT_PWD\x64\Debug"
	#QMAKE_POST_LINK += $(MKDIR) \"$$DESTDIR\data\\\"
	QMAKE_POST_LINK += COPY /y \"$$DLL_DIR\glew32d.dll\" \"$$DESTDIR\"
	QMAKE_POST_LINK += && COPY /y \"$$DLL_DIR\SDL2.dll\" \"$$DESTDIR\"
}

win64:CONFIG(release, debug|release) {
	LIBS += -L\"$$DAHLIA_VC\lib\x64\Release\" -lSDL2main -lSDL2 -lglew32 -lglu32 -lopengl32

	DLL_DIR = "$$DAHLIA_VC\dll\x64\Release"
	DESTDIR = "$$OUT_PWD\builds\x64\Release"
	MOC_DIR = "$$OUT_PWD\x64\Release"
	#QMAKE_POST_LINK += $(MKDIR) \"$$DESTDIR\data\\\"
	QMAKE_POST_LINK += COPY /y \"$$DLL_DIR\glew32.dll\" \"$$DESTDIR\"
	QMAKE_POST_LINK += && COPY /y \"$$DLL_DIR\SDL2.dll\" \"$$DESTDIR\"
}

win* {
	#DIRS = data

	#for(DIR, DIRS) {
	#	 mkcommands += \"$$DESTDIR/$$DIR\"
	#}

	#createDirs.commands = $(MKDIR) $$mkcommands
	#first.depends += createDirs
	#QMAKE_EXTRA_TARGETS += createDirs

	#QMAKE_POST_LINK += && COPY /y \"data\color.frag\" \"$${DESTDIR}\data\\\"
	#QMAKE_POST_LINK += && COPY /y \"data\color.vert\" \"$${DESTDIR}\data\\\"

	# Copy data directory
	QMAKE_POST_LINK += && xcopy /s /q /y /i \"$$PWD/data\" \"$${DESTDIR}\data\\\"

	# Copy data directory
	#copydata.commands = $(COPY_DIR) \"$$PWD/data\" \"$${DESTDIR}\data\\\"
	#first.depends = $(first) copydata
	#export(first.depends)
	#export(copydata.commands)
	#QMAKE_EXTRA_TARGETS += first copydata
}

#HEADERS += \



